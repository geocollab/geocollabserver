# GeoCollabServer

All docs can be found at <https://geocollab.gitlab.io>

Goals of the server side application:

- Enable clients to easily query information so no or nearly no client logic is required.
- Provide a real time collaboration interface.
- Enable admins to set up their own servers and use cross-server collaboration.
- Use the [Open-API-Spec](https://www.openapis.org/) to generate the REST-API. 

Apart from these main goals we aim to deliver a scalable and easily configurable application. 
